package es.singavan.arete.users.config

import com.google.auth.oauth2.GoogleCredentials
import com.google.cloud.firestore.Firestore
import com.google.cloud.firestore.FirestoreOptions
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.auth.FirebaseAuth
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties('google')
class FirebaseConfig {

    String projectId

    FirebaseApp firebaseApp

    @Bean
    FirebaseApp getFirebaseApp() {
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.getApplicationDefault())
                .setProjectId(projectId)
                .build()

        firebaseApp = FirebaseApp.initializeApp(options)
        firebaseApp
    }

    @Bean
    Firestore getFirestoreDb() {
        FirestoreOptions.getDefaultInstance().toBuilder().setTimestampsInSnapshotsEnabled(true).
                setProjectId(projectId).build().service
    }

    @Bean
    FirebaseAuth getFirebaseAuth() {
        if (firebaseApp) {
            FirebaseAuth.getInstance(firebaseApp)
        } else {
            FirebaseAuth.getInstance(getFirebaseApp())
        }
    }

}
