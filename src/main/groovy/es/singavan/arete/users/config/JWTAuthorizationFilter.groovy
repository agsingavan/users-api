package es.singavan.arete.users.config


import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import com.google.firebase.auth.FirebaseToken
import groovy.util.logging.Slf4j
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter

import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Slf4j
class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private static final String HEADER_AUTHORIZATION_KEY = "Authorization"
    private static final String TOKEN_BEARER_PREFIX = "Bearer "

    JWTAuthorizationFilter(AuthenticationManager authManager) {
        super(authManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        String header = req.getHeader(HEADER_AUTHORIZATION_KEY)
        if (header == null || !header.startsWith(TOKEN_BEARER_PREFIX)) {
            chain.doFilter(req, res)
        } else {
            UsernamePasswordAuthenticationToken authentication = getAuthentication(req)
            SecurityContextHolder.getContext().setAuthentication(authentication)
            chain.doFilter(req, res)
        }
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_AUTHORIZATION_KEY)
        if (token) {
            try {
                FirebaseToken decodedToken = FirebaseAuth.getInstance().verifyIdToken(
                        token.replace(TOKEN_BEARER_PREFIX, ""))
                String uid = decodedToken.getUid()
                if (uid != null) {
                    return new UsernamePasswordAuthenticationToken(decodedToken, token, new ArrayList<>())
                }
            } catch (FirebaseAuthException firebaseAuthException) {
                log.info(firebaseAuthException.message)
            }
        }
    }
}

