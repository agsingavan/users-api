package es.singavan.arete.users.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import org.springframework.security.task.DelegatingSecurityContextAsyncTaskExecutor

@Configuration
@EnableAsync
class AsyncConfig {

    @Bean
    ThreadPoolTaskExecutor threadPoolTaskExecutor() {
        new ThreadPoolTaskExecutor()
    }

    @Bean
    DelegatingSecurityContextAsyncTaskExecutor taskExecutor(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
        new DelegatingSecurityContextAsyncTaskExecutor(threadPoolTaskExecutor)
    }
}
