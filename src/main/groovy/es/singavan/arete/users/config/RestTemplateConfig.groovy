package es.singavan.arete.users.config


import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.web.client.RestTemplate

@Configuration
class RestTemplateConfig {

    @Bean
    RestTemplate getRestTemplate() {
        new RestTemplate().with {
            requestFactory = new HttpComponentsClientHttpRequestFactory()
            it
        }
    }

}
