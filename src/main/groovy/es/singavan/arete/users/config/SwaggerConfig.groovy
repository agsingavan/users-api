package es.singavan.arete.users.config

import com.fasterxml.classmate.TypeResolver
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.http.ResponseEntity
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.ApiKey
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

/**
 * Config for swagger
 */
@Configuration
@EnableSwagger2
@ConfigurationProperties('swagger')
@ComponentScan()
class SwaggerConfig {

    Boolean active

    String groupName

    String title

    String description

    @Autowired
    private TypeResolver typeResolver

    /**
     * Create Swagger Api configuration
     *
     * @return Swagger Docket
     */
    @Bean
    Docket documentApi() {
        active ?
                new Docket(DocumentationType.SWAGGER_2)
                        .groupName(groupName)
                        .apiInfo(apiInfo())
                        .ignoredParameterTypes(MetaClass)
                        .select()
                        .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                        .paths(PathSelectors.any())
                        .build()
                        .pathMapping("/")
                        .genericModelSubstitutes(ResponseEntity.class)
                        .genericModelSubstitutes(Optional.class)
                        .securitySchemes([apiKey()])
                        .useDefaultResponseMessages(false)
                        .forCodeGeneration(true)
                : null
    }

    private ApiKey apiKey() {
        new ApiKey("Bearer", "Authorization", "header")
    }

    /**
     * Generate Api Info
     *
     * @return Swagger API Info
     */
    private ApiInfo apiInfo() {
        new ApiInfoBuilder()
                .title(title)
                .description(description)
                .build()
    }
}
