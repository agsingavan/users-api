package es.singavan.arete.users.domain.usecases

import es.singavan.arete.users.domain.model.User
import es.singavan.arete.users.domain.services.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class GetUserByIdUseCaseService {

    @Autowired
    UserService userService

    User execute(String userId) {
        userService.getUserById(userId)
    }

}
