package es.singavan.arete.users.domain.model

enum UserTypeEnum {
    TUTOR,
    ASSISTANT
}