package es.singavan.arete.users.domain.exceptions

class UserNotFoundException extends RuntimeException {

    UserNotFoundException(String message){
        super(message)
    }

}
