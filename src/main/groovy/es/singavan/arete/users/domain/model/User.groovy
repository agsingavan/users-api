package es.singavan.arete.users.domain.model

import groovy.transform.ToString

@ToString(includePackage = false, includeFields = true, includeNames = true)
class User {

    String id
    String email
    String firstname
    String surname1
    String surname2
    Date birthDate
    UserTypeEnum userTypeEnum

    boolean isTutor() {
        userTypeEnum == UserTypeEnum.TUTOR
    }

    boolean isAssistant() {
        userTypeEnum == UserTypeEnum.ASSISTANT
    }

}
