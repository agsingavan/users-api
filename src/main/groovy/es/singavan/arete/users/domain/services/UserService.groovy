package es.singavan.arete.users.domain.services

import es.singavan.arete.users.domain.model.User
import es.singavan.arete.users.infraestructure.dao.UserDao
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
@Slf4j
class UserService {

    @Autowired
    UserDao userDao

    User getUserById(String id) {
        userDao.getUserById(id)
    }
}
