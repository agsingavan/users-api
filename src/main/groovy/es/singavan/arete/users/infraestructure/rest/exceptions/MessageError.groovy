package es.singavan.arete.users.infraestructure.rest.exceptions

class MessageError {
    
    String code
    
    String description

    String detail
}
