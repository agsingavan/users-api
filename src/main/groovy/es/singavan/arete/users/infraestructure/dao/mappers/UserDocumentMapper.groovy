package es.singavan.arete.users.infraestructure.dao.mappers

import com.google.cloud.firestore.QueryDocumentSnapshot
import es.singavan.arete.users.domain.model.User
import es.singavan.arete.users.infraestructure.dao.documents.UserDocument

class UserDocumentMapper {

    static User mapToDomain(UserDocument userDocument) {
        new User(
                id: userDocument.id,
                email: userDocument.email,
                firstname: userDocument.firstname,
                surname1: userDocument.surname1,
                surname2: userDocument.surname2,
                birthDate: userDocument.birthDate,
                userTypeEnum: userDocument.userType ?
                        UserTypeDocumentMapper.mapToUserType(userDocument.userType) : null,
        )
    }

    static UserDocument mapToDocument(User user) {
        new UserDocument(
                id: user.id,
                email: user.email,
                firstname: user.firstname,
                surname1: user.surname1,
                surname2: user.surname2,
                birthDate: user.birthDate,
                userType: user.userTypeEnum ? UserTypeDocumentMapper.mapToString(user.userTypeEnum) : null,
        )
    }

    static List<User> mapToDomainList(List<QueryDocumentSnapshot> queryDocumentSnapshotList) {
        List<User> userList = null
        if (queryDocumentSnapshotList) {
            userList = []
            queryDocumentSnapshotList?.each {
                userList << mapToDomain(it.toObject(UserDocument))
            }
        }
        userList
    }

}
