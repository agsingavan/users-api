package es.singavan.arete.users.infraestructure.rest


import es.singavan.arete.users.domain.usecases.GetUserByIdUseCaseService
import es.singavan.arete.users.infraestructure.rest.contracts.UserControllerContract
import es.singavan.arete.users.infraestructure.rest.dto.UserDto
import es.singavan.arete.users.infraestructure.rest.mappers.UserDtoMapper
import groovy.util.logging.Slf4j
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(value = '/users',
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE)
@Api
@Slf4j
class UserController implements UserControllerContract {

    @Autowired
    GetUserByIdUseCaseService getUserByIdUseCaseService

    @GetMapping(path = '/{userId}')
    @ResponseStatus(HttpStatus.OK)
    UserDto getUser(@PathVariable(required = true) String userId) {
        Authentication authentication = SecurityContextHolder.context.authentication

        UserDtoMapper.mapToDto(getUserByIdUseCaseService.execute(userId))
    }

}
