package es.singavan.arete.users.infraestructure.rest.mappers

import es.singavan.arete.users.domain.model.User
import es.singavan.arete.users.infraestructure.rest.dto.UserDto
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Slf4j
class UserDtoMapper {

    static UserDto mapToDto(User user) {
        if (user) {
            new UserDto(
                    id: user.id,
                    email: user.email,
                    firstname: user.firstname,
                    surname1: user.surname1,
                    surname2: user.surname2,
                    birthDate: user.birthDate,
                    userType: user.userTypeEnum ? UserTypeDtoMapper.mapToDto(user.userTypeEnum) : null,
            )
        }
    }

    static User mapToDomain(UserDto userDto) {
        if (userDto) {
            new User(
                id: userDto.id,
                email: userDto.email,
                firstname: userDto.firstname,
                surname1: userDto.surname1,
                surname2: userDto.surname2,
                birthDate: userDto.birthDate,
                userTypeEnum: userDto.userType ? UserTypeDtoMapper.mapToDomain(userDto.userType) : null,
            )
        }
    }


}
