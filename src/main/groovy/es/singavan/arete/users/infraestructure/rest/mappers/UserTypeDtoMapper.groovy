package es.singavan.arete.users.infraestructure.rest.mappers

import es.singavan.arete.users.domain.model.UserTypeEnum
import es.singavan.arete.users.infraestructure.rest.dto.UserTypeEnumDto
import groovy.util.logging.Slf4j

@Slf4j
class UserTypeDtoMapper {

    static UserTypeEnum mapToDomain(UserTypeEnumDto userType) {
        UserTypeEnum.valueOf(userType.name())
    }

    static UserTypeEnumDto mapToDto(UserTypeEnum userTypeEnum) {
        UserTypeEnumDto.valueOf(userTypeEnum.name())
    }

}
