package es.singavan.arete.users.infraestructure.dao.documents

class UserDocument {

    static final String COLLECTION = 'users'

    static final String ID = 'id'
    static final String EMAIL = 'email'
    static final String FIRSTNAME = 'firstname'
    static final String SURNAME1 = 'surname1'
    static final String SURNAME2 = 'surname2'
    static final String USER_TYPE = 'userType'
    static final String BIRTH_DATE = 'birthDate'

    String id
    String email
    String firstname
    String surname1
    String surname2
    Date birthDate
    String userType

}
