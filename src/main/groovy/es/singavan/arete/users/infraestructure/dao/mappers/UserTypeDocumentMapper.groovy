package es.singavan.arete.users.infraestructure.dao.mappers

import es.singavan.arete.users.domain.model.UserTypeEnum
import org.springframework.stereotype.Component

class UserTypeDocumentMapper {

    static UserTypeEnum mapToUserType(String userType) {
        UserTypeEnum userTypeEnum = null
        if (userType) {
            switch (userType) {
                case UserTypeEnum.ASSISTANT.name(): userTypeEnum = UserTypeEnum.ASSISTANT
                    break
                case UserTypeEnum.TUTOR.name(): userTypeEnum = UserTypeEnum.TUTOR
                    break
                default:
                    throw new RuntimeException("User type $userType not exist")
            }
        }
        userTypeEnum
    }

    static String mapToString(UserTypeEnum userTypeEnum) {
        userTypeEnum?.name()
    }

}
