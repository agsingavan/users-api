package es.singavan.arete.users.infraestructure.rest.dto

import groovy.transform.ToString

@ToString(includePackage = false, includeFields = true, includeNames = true)
class UserDto {
    String id
    String email
    String firstname
    String surname1
    String surname2
    Date birthDate
    UserTypeEnumDto userType
}
