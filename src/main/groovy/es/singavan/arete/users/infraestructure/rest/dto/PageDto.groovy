package es.singavan.arete.users.infraestructure.rest.dto

import groovy.transform.ToString

@ToString(includePackage = false, includeFields = true, includeNames = true)
class PageDto<T> {

    List<T> elements

}