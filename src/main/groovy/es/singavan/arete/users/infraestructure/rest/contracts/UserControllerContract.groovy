package es.singavan.arete.users.infraestructure.rest.contracts


import es.singavan.arete.users.infraestructure.rest.dto.UserDto
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import io.swagger.annotations.Authorization

interface UserControllerContract {

    @ApiOperation(
            nickname = 'Retrieve User By Id',
            tags = ["Users"],
            response = UserDto,
            value = 'user by id',
            authorizations = [@Authorization(value = "Bearer")]
    )
    @ApiResponses(value = [
            @ApiResponse(code = 200, message = 'Success', response = UserDto),
            @ApiResponse(code = 401, message = 'Unauthorized'),
            @ApiResponse(code = 404, message = 'Not Found'),
            @ApiResponse(code = 500, message = 'Failure')
    ])
    UserDto getUser(String userId);

}