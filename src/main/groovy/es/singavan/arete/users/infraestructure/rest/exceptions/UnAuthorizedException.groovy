package es.singavan.arete.users.infraestructure.rest.exceptions


class UnAuthorizedException extends RuntimeException {

    UnAuthorizedException(String message) {
        super(message)
    }

}
