package es.singavan.arete.users.infraestructure.rest.exceptions


import es.singavan.arete.users.domain.exceptions.UserNotFoundException

import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler


@ControllerAdvice
@Slf4j
class ControlAdviceHandler {

    @Autowired
    MessageErrorFactory messageErrorFactory

    @ExceptionHandler(value = [UserNotFoundException, es.singavan.arete.users.domain.exceptions.FeatureElementNotFoundException,
            FeatureNotFoundException, EnterpriseDataNotFoundException, RequestCotenantNotFound,
            RequestAnalysisNotFoundException, AssetNotFoundException, es.singavan.arete.users.domain.exceptions.CandidatureNotFoundException,
            AnalysisNotFoundException, TenantRequestDocumentationNotFoundException, es.singavan.arete.users.domain.exceptions.IncidenceNotFoundException])
    protected ResponseEntity<Object> handleNotFound(RuntimeException ex) {
        log.error(ex.message, ex)
        new ResponseEntity<MessageError>(getMessageError(ex), HttpStatus.NOT_FOUND)
    }

    @ExceptionHandler(value = [InvalidDniException, AssetDataRequiredException, SharedTypeInvalidException,
            es.singavan.arete.users.domain.exceptions.DniRequiredException])
    protected ResponseEntity<Object> handleBadRequestException(RuntimeException ex) {
        log.error(ex.message, ex)
        new ResponseEntity<MessageError>(getMessageError(ex), HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(value = [UnAuthorizedException])
    protected ResponseEntity<Object> handleUnAuthorizedException(RuntimeException ex) {
        log.error(ex.message, ex)
        new ResponseEntity<MessageError>(getMessageError(ex), HttpStatus.UNAUTHORIZED)
    }

    @ExceptionHandler(value = [ExistDniException, OperationIncorrectException, UserYetRegister,
        es.singavan.arete.users.domain.exceptions.NoTenantUserTypeException, es.singavan.arete.users.domain.exceptions.UserConflictException, es.singavan.arete.users.domain.exceptions.AnalysisAlreadyRegisteredException,
        DuplicatedRequestCotenantException, es.singavan.arete.users.domain.exceptions.NumberOfAcceptedCotenantRequestsExceeded, TenantRequestDocumentationException,
        InvalidEmailException, UserIsAlreadyGuarantorException,
        UserHasAlreadyCotenantsList])
    protected ResponseEntity<Object> handleConflictException(RuntimeException ex) {
        log.error(ex.message, ex)
        new ResponseEntity<MessageError>(getMessageError(ex), HttpStatus.CONFLICT)
    }

    @ExceptionHandler(value = [MethodArgumentNotValidException])
    ResponseEntity<MessageError> handleValidationException(MethodArgumentNotValidException pe) {
        new ResponseEntity<MessageError>(messageErrorFactory.getMessageError(pe), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RestResourceAccessException.class)
    protected ResponseEntity<Object> handleRestResourceAccessException(RuntimeException ex) {
        log.error(ex.message, ex)
        new ResponseEntity<MessageError>(getMessageError(ex), HttpStatus.INTERNAL_SERVER_ERROR)
    }

    private MessageError getMessageError(RuntimeException runtimeException) {
        messageErrorFactory.getMessageError(runtimeException).with {
            detail = runtimeException.message
            it
        }
    }

}
