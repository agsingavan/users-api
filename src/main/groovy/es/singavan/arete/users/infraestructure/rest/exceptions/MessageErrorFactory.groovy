package es.singavan.arete.users.infraestructure.rest.exceptions


import es.singavan.arete.users.domain.exceptions.UserNotFoundException

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.web.bind.MethodArgumentNotValidException

@Component
@ConfigurationProperties('app.errors')
class MessageErrorFactory {

    MessageError invalidDni

    MessageError existDni

    MessageError userNotFound

    MessageError featureNotFound

    MessageError featureElementNotFound

    MessageError operationIncorrect

    MessageError userYetRegister

    MessageError assetDataRequired

    MessageError sharedTypeInvalid

    MessageError unAuthorized

    MessageError enterpriseDataNotFound

    MessageError noTenantUserTypeException

    MessageError requestCotenantNotFound

    MessageError userTypeConflictException

    MessageError requestAnalysisNotFound

    MessageError methodArgumentNotValid

    MessageError assetNotFound

    MessageError analysisAlreadyRegistered

    MessageError candidatureNotFound

    MessageError analysisNotFound

    MessageError duplicatedRequestCotenant

    MessageError numberOfAcceptedCotenantRequestsExceeded

    MessageError tenantRequestDocumentationException

    MessageError tenantRequestDocumentationNotFoundException

    MessageError restResourceAccessException

    MessageError incidenceNotFoundException

    MessageError invalidEmailException

    MessageError userIsAlreadyGuarantorException

    MessageError userHasAlreadyCotenantsList
    
    MessageError dniRequiredException

    MessageError getMessageError(RuntimeException runtimeException) {
        MessageError messageError
        switch (runtimeException) {
            case InvalidDniException: messageError = invalidDni
                break
            case ExistDniException: messageError = existDni
                break
            case UserNotFoundException: messageError = userNotFound
                break
            case FeatureNotFoundException: messageError = featureNotFound
                break
            case es.singavan.arete.users.domain.exceptions.FeatureElementNotFoundException: messageError = featureElementNotFound
                break
            case OperationIncorrectException: messageError = operationIncorrect
                break
            case UserYetRegister: messageError = userYetRegister
                break
            case AssetDataRequiredException: messageError = assetDataRequired
                break
            case SharedTypeInvalidException: messageError = sharedTypeInvalid
                break
            case UnAuthorizedException: messageError = unAuthorized
                break
            case EnterpriseDataNotFoundException: messageError = enterpriseDataNotFound
                break
            case RequestCotenantNotFound: messageError = requestCotenantNotFound
                break
            case es.singavan.arete.users.domain.exceptions.NoTenantUserTypeException: messageError = noTenantUserTypeException
                break
            case es.singavan.arete.users.domain.exceptions.AnalysisAlreadyRegisteredException: messageError = analysisAlreadyRegistered
                break
            case es.singavan.arete.users.domain.exceptions.UserConflictException: messageError = userTypeConflictException
                break
            case AssetNotFoundException: messageError = assetNotFound
                break
            case es.singavan.arete.users.domain.exceptions.CandidatureNotFoundException: messageError = candidatureNotFound
                break
            case RequestAnalysisNotFoundException: messageError = requestAnalysisNotFound
                break
            case AnalysisNotFoundException: messageError = analysisNotFound
                break
            case DuplicatedRequestCotenantException: messageError = duplicatedRequestCotenant
                break
            case es.singavan.arete.users.domain.exceptions.NumberOfAcceptedCotenantRequestsExceeded: messageError = numberOfAcceptedCotenantRequestsExceeded
                break
            case TenantRequestDocumentationException: messageError = tenantRequestDocumentationException
                break
            case TenantRequestDocumentationNotFoundException: messageError = tenantRequestDocumentationNotFoundException
                break
            case RestResourceAccessException: messageError = restResourceAccessException
                break
            case IncidenceNotFoundException: messageError = incidenceNotFoundException
                break
            case InvalidEmailException: messageError = invalidEmailException
                break
            case UserIsAlreadyGuarantorException: messageError = userIsAlreadyGuarantorException
                break
            case UserHasAlreadyCotenantsList: messageError = userHasAlreadyCotenantsList
                break
            case es.singavan.arete.users.domain.exceptions.DniRequiredException: messageError = dniRequiredException
                break
            default:
                messageError = new MessageError(description: 'Uncategorized error')
        }
        messageError
    }

    MessageError getMessageError(MethodArgumentNotValidException methodArgumentNotValidException) {
        methodArgumentNotValid.with {
            detail = methodArgumentNotValidException.message
            it
        }
    }

}
