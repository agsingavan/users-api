package es.singavan.arete.users.infraestructure.rest.dto

enum UserTypeEnumDto {
    TUTOR,
    ASSISTANT
}