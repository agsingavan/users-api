package es.singavan.arete.users.infraestructure.dao


import com.google.cloud.firestore.Firestore
import com.google.cloud.firestore.QueryDocumentSnapshot
import es.singavan.arete.users.domain.model.User
import es.singavan.arete.users.infraestructure.dao.documents.UserDocument
import es.singavan.arete.users.infraestructure.dao.mappers.UserDocumentMapper
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
@Slf4j
class UserDao {

    @Autowired
    Firestore firestore

    User save(User user) {
        UserDocument userDocument = UserDocumentMapper.mapToDocument(user)
        firestore.collection(UserDocument.COLLECTION).document().set(mapProperties(userDocument)).get()
        user
    }

    private static Map<String, Object> mapProperties(UserDocument userDocument) {
        Map<String, Object> map = new HashMap<>()
        map.put(UserDocument.ID, userDocument.id)
        map.put(UserDocument.EMAIL, userDocument.email)
        map.put(UserDocument.FIRSTNAME, userDocument.firstname)
        map.put(UserDocument.SURNAME1, userDocument.surname1)
        map.put(UserDocument.SURNAME2, userDocument.surname2)
        map.put(UserDocument.USER_TYPE, userDocument.userType)
        map.put(UserDocument.BIRTH_DATE, userDocument.birthDate)
        map
    }

    User getUserById(String userId) {
        User user = null
        List<QueryDocumentSnapshot> documentSnapshotList = firestore.collection(UserDocument.COLLECTION).
                whereEqualTo(UserDocument.ID, userId).get().get().documents
        if (documentSnapshotList) {
            user = UserDocumentMapper.mapToDomain(documentSnapshotList[0].toObject(UserDocument))
        }
        user
    }

}
